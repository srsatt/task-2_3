//
//  AppDelegate.m
//  Task2_3
//
//  Created by Павел on 01.10.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "AppDelegate.h"
#import "Person.h"
@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    
    
    //создание и заполнение персоны
    Person *Vasya = [[Person alloc] initWithName: @"Vasilii" LastName: @"Ivanov" Birthdate : @"10/11/1992"];
    NSLog(@"%@", Vasya);
    [Vasya fullFieldsWithName:(nil) LastName: (nil) Birthdate: @"30/11/1943"];
    
    NSLog(@"%@", Vasya);
    
    
    
    [Vasya savePersonAsDictioanry];
    Person *Vasya2=[[Person alloc]initFromDictionary: [[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop"] stringByAppendingPathComponent:@"NA Ivanov"]];
    
     NSLog(@"%@", Vasya2);
    
    
    NSData *VasyaData = [NSKeyedArchiver archivedDataWithRootObject:Vasya];
    Person *NewVasya = [NSKeyedUnarchiver unarchiveObjectWithData:VasyaData];
    NSLog(@"unarchived Person %@", NewVasya);
     
    
    /*
     
     
    //считывание с json файла
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"names" ofType:@"json"];
    NSData *jsonLoad=[[NSData alloc] initWithContentsOfFile:filePath];
    NSMutableDictionary *peoples=[[NSMutableDictionary alloc] init];
    peoples=[NSJSONSerialization JSONObjectWithData:jsonLoad options: 0 error:nil];
   
    //создание словаря с персонами
    NSMutableDictionary *persons=[[NSMutableDictionary alloc] init];
    
    int personID=1;
    for(NSMutableDictionary *human in peoples){
        NSLog(@"%@",human);
        Person *tempPerson=[[Person alloc] initWithName: human[@"FirstName"] LastName: human[@"LastName"] Birthdate : human[@"BirthDate"] ];
        [persons setObject:tempPerson forKey: [NSString stringWithFormat:@"%d",personID]];
        personID++;
    }
    NSDictionary *personsDict=[[NSDictionary alloc] initWithDictionary:persons];
    NSLog(@"%@", personsDict);
   //создал словарь из людей типа person, с ключами ID. пока пусть будет
    */
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
