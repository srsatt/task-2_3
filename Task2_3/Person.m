//
//  Person.m
//  task_2-3
//
//  Created by Павел on 29.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import "Person.h"
NSString * const FirstNameKey=@"FirstName";
NSString * const LastNameKey=@"LastName";
NSString * const BirthDateKey=@"BirthDate";

NSString *name;
NSString *lastname;
NSString *birthdate;

@implementation Person

#pragma mark initiation

-(instancetype)initWithName: (NSString*) firstName LastName: (NSString*) lastName Birthdate : (NSString*)birthDate {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"\"d/m/yyyy\""];
    NSDate *Date = [format dateFromString:birthDate];
    //[format release]; //это строка была в примерах но с ней не компилится. Если ее нету, то это ошибка?
//    это старый objc, без ARC
    if (!(self = [self init]))  return nil;
    _firstName=firstName;
    _lastName=lastName;
    _birthDate=Date;
    return self;
    
}

-(instancetype)initFromDictionary: (NSString*) filepath {
    NSDictionary *person=[[NSDictionary alloc] initWithContentsOfFile: filepath];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"d/m/yyyy"];
    NSDate *Date = [format dateFromString:person[@"BirthDate"]];
    if (!(self = [self init]))  return nil;
    if([person[FirstNameKey] isEqualToString:@"NA"]) _firstName=nil;
    else _firstName=person[FirstNameKey];
    if([person[LastNameKey] isEqualToString:@"_NA"]) _lastName=nil;
    else _lastName=person[LastNameKey];
    if([person[BirthDateKey] isEqualToString:@"NA"]) _birthDate=nil;
    else  _birthDate=Date;
   
    NSLog(@"Loading status: successful");
    return self;
    
}


#pragma mark other methods
- (NSString *)description{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"d-m-yyyy"];
    return [NSString stringWithFormat:@" \n Person's first name is  %@ \n Person's last name is %@ \n and person's birthdate is  %@", self.firstName, self.lastName, [format stringFromDate:self.birthDate]];
    //[format release]; --//--
}

-(instancetype)fullFieldsWithName: (NSString*) FirstName LastName: (NSString*) LastName Birthdate : (NSString*)BirthDate{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"d/m/yyyy"];
    NSDate *Date = [format dateFromString:BirthDate];
    _firstName=FirstName;
    _lastName=LastName;
    _birthDate=Date;
    return self;
}

-(void)savePersonAsDictioanry {
     NSDateFormatter *format = [[NSDateFormatter alloc] init];
     [format setDateFormat:@"d/m/yyyy"];
//   TODO: ask me about the case
    if (!self.firstName)
    {name=@"NA ";}
        else {name=self.firstName;}
    
    if (!self.lastName)
    {lastname=@"_NA";}
        else {lastname=self.lastName;}
    
    if (!self.birthDate)
    {birthdate=@"NA";}
        else {birthdate=[format stringFromDate:self.birthDate];}
    
    
     NSLog(@"%@ %@", name, lastname);
    NSLog(@"%@",[[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop"] stringByAppendingPathComponent: [name stringByAppendingString: lastname]]);
     NSLog(@"%@ %@", name, lastname);
    
    NSDictionary *personDict=[[NSDictionary alloc] initWithObjects:@[ name, lastname, birthdate,]  forKeys:@[FirstNameKey,LastNameKey,BirthDateKey]];
    [personDict writeToFile:[[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop"] stringByAppendingPathComponent: [name stringByAppendingString: lastname]] atomically:YES] ;
    NSLog(@"Saved");
    
}

#pragma mark encoding/decoding




- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    if (!(self = [super init])) return nil;
    _firstName=[aDecoder decodeObjectForKey:FirstNameKey];
    _lastName=[aDecoder decodeObjectForKey:LastNameKey];
    _birthDate = [aDecoder decodeObjectForKey:BirthDateKey];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.firstName forKey:FirstNameKey];
    [aCoder encodeObject:self.lastName forKey:LastNameKey];
    [aCoder encodeObject:self.birthDate forKey:BirthDateKey];
}





@end