//
//  Person.h
//  task_2-3
//
//  Created by Павел on 29.09.14.
//  Copyright (c) 2014 mipt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject <NSCoding>
//FIxed - а это нужно для красоты, или из-за этого потом ошибки могут возникнуть?
//  стиль такой, поправьте тоже самое в предыдущем задании
@property NSString *firstName;
@property NSString *lastName;
@property NSDate *birthDate;


-(instancetype)initWithName: (NSString*) firstName LastName: (NSString*) lastName Birthdate : (NSString*)birthDate;
-(instancetype)fullFieldsWithName: (NSString*) firstName LastName: (NSString*) lastName Birthdate : (NSString*)birthDate ;
-(void)savePersonAsDictioanry;
-(instancetype)initFromDictionary: (NSString*) filepath;
@end
